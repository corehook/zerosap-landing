var gulp = require('gulp');
var watch = require('gulp-watch');
var haml = require('gulp-ruby-haml');

// Compile Haml into HTML
gulp.task('haml', function() {

  watch('./**/*.haml', function () {
    gulp.src('./**/*.haml')
      .pipe(watch('./**/*.haml'))
      .pipe(haml())
      .pipe(gulp.dest('./'));
  });

});
